#!/usr/bin/env node

/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

const execAsync = require('util').promisify(require('child_process').exec);

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const username = process.env.USERNAME, password = process.env.PASSWORD;
    const TEST_TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 10000;

    let browser, app;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    async function dockerLogout() {
        execSync(`docker logout ${app.fqdn}`, EXEC_ARGS);
    }

    async function dockerLoginFails() {
        try {
            await execAsync(`docker login ${app.fqdn} -u ${username}x -p ${password}y`, { encoding: 'utf8' });
            throw 'ooops, docker login succeed';
        } catch (error) {
            expect(error.stderr).to.contain('403 Forbidden');
        }
    }

    async function dockerLogin() {
        const { stdout:stdout1 } = await execAsync(`docker login ${app.fqdn} -u ${username} -p ${password}`);
        if (!stdout1.includes('Login Succeeded')) throw 'Authentication failed';
    }

    async function pushImage() {
        execSync('docker pull hello-world', EXEC_ARGS);
        execSync(`docker tag hello-world ${app.fqdn}/hello-world`, EXEC_ARGS);
        execSync(`docker push ${app.fqdn}/hello-world`, EXEC_ARGS);
    }

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    async function login(alreadyLoggedIn = true) {
        await browser.get(`https://${app.fqdn}/login`);
        await waitForElement(By.id('loginProceedButton'));
        await browser.findElement(By.id('loginProceedButton')).click();

        if (!alreadyLoggedIn) {
            await waitForElement(By.id('inputUsername'));
            await browser.findElement(By.id('inputUsername')).sendKeys(username);
            await browser.findElement(By.id('inputPassword')).sendKeys(password);
            await browser.findElement(By.id('loginSubmitButton')).click();
        }

        await browser.wait(until.elementLocated(By.xpath('//*[contains(text(), "Docker Registry")]')), TEST_TIMEOUT);
    }

    async function logout() {
        await browser.get(`https://${app.fqdn}/logout`);
        await waitForElement(By.id('loginProceedButton'));
    }

    async function checkImageExists() {
        await browser.get(`https://${app.fqdn}`);
        await browser.wait(until.elementLocated(By.xpath('//div[contains(text(), "1 images")]')), TEST_TIMEOUT); // the actual repo name is hidden in some material UI!
    }

    async function checkStaticPage() {
        await browser.get(`https://${app.fqdn}`);
        await browser.wait(until.elementLocated(By.xpath('//h1[contains(text(), "Docker Registry Cloudron App")]')), TEST_TIMEOUT);
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', function () { execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);
    it('can logout with docker', dockerLogout);
    it('cannot login with bad password', dockerLoginFails);
    it('can login with docker', dockerLogin);
    it('can push image with docker', pushImage);
    it('can logout with docker', dockerLogout);
    it('can login', login.bind(null, false));
    it('can logout', logout);

    it('can restart app', function () { execSync(`cloudron restart --app ${app.id}`); });
    it('can login', login);
    it('check image exists', checkImageExists);
    it('can logout', logout);

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('restore app', function () {
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
        execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });
    it('can login', login);
    it('check image exists', checkImageExists);
    it('can logout', logout);

    it('move to different location', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
    });
    it('can get app information', getAppInfo);
    it('can login', login);
    it('check image exists', checkImageExists);
    it('can logout', logout);

    it('can get app information', getAppInfo);

    it('uninstall app', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // no sso
    it('install app', function () { execSync(`cloudron install --no-sso --location ${LOCATION}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);
    it('can logout with docker', dockerLogout);
    it('can push image with docker', pushImage);
    it('check static page', checkStaticPage);

    it('uninstall app', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // test update
    it('can install app', function () { execSync(`cloudron install --appstore-id com.docker.registry --location ${LOCATION}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);
    it('can logout with docker', dockerLogout);
    it('can login with docker', dockerLogin);
    it('can push image with docker', pushImage);
    it('can logout with docker', dockerLogout);

    it('can update', function () { execSync(`cloudron update --app ${app.id}`, EXEC_ARGS); });
    it('can logout with docker', dockerLogout);
    it('cannot login with bad password', dockerLoginFails);
    it('can login', login);
    it('check image exists', checkImageExists);
    it('can logout', logout);

    it('uninstall app', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });
});
