FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code
WORKDIR /app/code

ENV BUILDTAGS include_oss include_gcs

# Distribution. Instructions adapted from BUILDING.txt in the distribution's repo

# renovate: datasource=github-releases depName=distribution/distribution versioning=semver extractVersion=^v(?<version>.+)$
ARG REGISTRY_VERSION=2.8.3

RUN curl -L https://github.com/distribution/distribution/releases/download/v${REGISTRY_VERSION}/registry_${REGISTRY_VERSION}_linux_amd64.tar.gz | tar zxvf -

# Docker Registry UI (used with proxy auth)

# renovate: datasource=github-releases depName=Joxit/docker-registry-ui versioning=semver
ARG UI_VERSION=2.5.7

RUN mkdir /app/code/ui && cd /app/code/ui && \
    curl -L https://github.com/Joxit/docker-registry-ui/archive/${UI_VERSION}.tar.gz | tar -zxvf - --strip-components=1 && \
    cp /app/code/ui/dist/index.html /app/code/ui/dist/index.html.original && \
    ln -sf /run/registry/ui/index.html /app/code/ui/dist/index.html

# Simple information page (used when proxy auth disabled)
COPY frontend-customauth/ /app/code/frontend-customauth

# nginx
RUN rm -rf /var/log/nginx && ln -s /run/nginx /var/log/nginx

# Add supervisor configs
COPY supervisor/* /etc/supervisor/conf.d/
RUN ln -sf /run/registry/supervisord.log /var/log/supervisor/supervisord.log

COPY registry-ui.sh config-example.yml start.sh nginx.conf.template /app/code/

CMD [ "/app/code/start.sh" ]

