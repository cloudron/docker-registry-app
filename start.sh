#!/bin/bash

set -eu

mkdir -p /app/data/storage /run/nginx /run/registry/ui

if [[ ! -f /app/data/config.yml ]]; then
    cp /app/code/config-example.yml /app/data/config.yml
    yq eval -i ".http.secret=$(pwgen -1s 32)" /app/data/config.yml
fi

if [[ ! -f /app/data/registry-ui.sh ]]; then
    cp /app/code/registry-ui.sh /app/data/registry-ui.sh
fi

yq eval -i ".redis.addr=\"${CLOUDRON_REDIS_HOST}:${CLOUDRON_REDIS_PORT}\"" /app/data/config.yml 
yq eval -i ".redis.password=\"${CLOUDRON_REDIS_PASSWORD}\"" /app/data/config.yml 

# https://github.com/Joxit/docker-registry-ui/blob/main/bin/90-docker-registry-ui.sh
echo "=> Fixing UI config"
export REGISTRY_TITLE="Cloudron Registry"
export CATALOG_ELEMENTS_LIMIT=100000
export SHOW_CONTENT_DIGEST=true
export SHOW_CATALOG_NB_TAGS=true
export HISTORY_CUSTOM_LABELS=false
export CATALOG_MIN_BRANCHES=1
export CATALOG_MAX_BRANCHES=1
export DEFAULT_REGISTRIES=''
export READ_ONLY_REGISTRIES=false
export USE_CONTROL_CACHE_HEADER=false
export CATALOG_DEFAULT_EXPANDED=false
export TAGLIST_ORDER="alpha-asc;num-desc"
export TAGLIST_PAGE_SIZE=100
export REGISTRY_SECURED=false

source /app/data/registry-ui.sh
sed -e "s,\${URL},${CLOUDRON_APP_ORIGIN}/v2ui," \
    -e "s,\${REGISTRY_TITLE},${REGISTRY_TITLE}," \
    -e "s,\${PULL_URL},${CLOUDRON_APP_DOMAIN}," \
    -e "s,\${SINGLE_REGISTRY},true," \
    -e "s,\${REGISTRY_URL},${CLOUDRON_APP_ORIGIN}," \
    -e "s,\${CATALOG_ELEMENTS_LIMIT},${CATALOG_ELEMENTS_LIMIT}," \
    -e "s,\${SHOW_CONTENT_DIGEST},${SHOW_CONTENT_DIGEST}," \
    -e "s,\${DEFAULT_REGISTRIES},${DEFAULT_REGISTRIES}," \
    -e "s,\${READ_ONLY_REGISTRIES},${READ_ONLY_REGISTRIES}," \
    -e "s,\${SHOW_CATALOG_NB_TAGS},${SHOW_CATALOG_NB_TAGS}," \
    -e "s,\${HISTORY_CUSTOM_LABELS},${HISTORY_CUSTOM_LABELS}," \
    -e "s,\${USE_CONTROL_CACHE_HEADER},${USE_CONTROL_CACHE_HEADER}," \
    -e "s,\${TAGLIST_ORDER},${TAGLIST_ORDER}," \
    -e "s,\${CATALOG_DEFAULT_EXPANDED},${CATALOG_DEFAULT_EXPANDED}," \
    -e "s,\${CATALOG_MIN_BRANCHES},${CATALOG_MIN_BRANCHES}," \
    -e "s,\${CATALOG_MAX_BRANCHES},${CATALOG_MAX_BRANCHES}," \
    -e "s,\${TAGLIST_PAGE_SIZE},${TAGLIST_PAGE_SIZE}," \
    -e "s,\${REGISTRY_SECURED},${REGISTRY_SECURED}," \
    /app/code/ui/dist/index.html.original > /run/registry/ui/index.html

grep -o 'THEME[A-Z_]*' /run/registry/ui/index.html | while read e; do
    [[ -n "${e}" ]] && sed -i "s,\${$e},$(printenv $e)," /run/registry/ui/index.html
done

# can be removed after upgrade
yq eval -i ".catalog.maxentries=100000" /app/data/config.yml

if [[ "$(yq eval '.storage.delete.enabled' /app/data/config.yml)" == "true" ]]; then
    sed -e "s,\${DELETE_IMAGES},true," -i /run/registry/ui/index.html
else
    sed -e "s,\${DELETE_IMAGES},false," -i /run/registry/ui/index.html
fi

if [[ -n "${CLOUDRON_PROXY_AUTH:-}" ]]; then
    cp nginx.conf.template /run/nginx/nginx.conf
else
    sed -e 's,root .*,root /app/code/frontend-customauth;,g' nginx.conf.template > /run/nginx/nginx.conf
fi

echo "=> Ensure permissions"
chown -R cloudron:cloudron /app/data

echo "=> Starting supervisor"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i docker-registry

