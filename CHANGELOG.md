[0.1.0]
* Initial version

[0.2.0]
* Make it work with proxy auth

[0.3.0]
* Add UI

[0.4.0]
* Show static page when not using proxy auth

[0.5.0]
* Run GC once a day

[0.6.0]
* Fix '413 Request Entity Too Large'

[1.0.0]
* Update docker registry UI to 1.5.2

[1.1.0]
* Update base image to v3
* Update docker registry UI to 1.5.4

[1.2.0]
* Fix delete config

[1.3.0]
* Update docker registry UI to 2.0.0

[1.3.1]
* Update docker registry UI to 2.0.1

[1.3.2]
* Update docker registry UI to 2.0.2

[1.3.3]
* Update docker registry UI to 2.0.4

[1.3.4]
* Update docker registry UI to 2.0.5

[1.3.5]
* Update docker registry UI to 2.0.6

[1.3.6]
* Update docker registry UI to 2.0.8

[1.3.7]
* Update docker registry UI to 2.0.9

[1.4.0]
* Update docker registry UI to 2.1.0
* [Full changelog](https://github.com/Joxit/docker-registry-ui/releases/tag/2.1.0)
* search bar: add shortcuts CRTL + F or F3 to select the search bar (f958365)
* default registries is set only when there is no registries (dd26bf6)

[1.5.0]
* Update docker registry to 2.8.1 - [changes](https://github.com/distribution/distribution/releases/tag/v2.8.0)
* Update docker registry UI to 2.2.0 - [changes](https://github.com/Joxit/docker-registry-ui/releases/tag/2.2.0)

[1.5.1]
* Update docker registry UI to 2.2.1 - [changes](https://github.com/Joxit/docker-registry-ui/releases/tag/2.2.1)

[1.6.0]
* [ui] Show delete images button
* [ui] Show content digest and tags

[1.6.1]
* Update docker registry UI to 2.2.2 - [changes](https://github.com/Joxit/docker-registry-ui/releases/tag/2.2.2)
* taglist: bug when listing multi-arch images (#260) (57a1cf9)

[1.6.2]
* Update docker registry UI to 2.3.0 - [changes](https://github.com/Joxit/docker-registry-ui/releases/tag/2.3.0)
* tag-list: missing details on images built by buildah (#264) (fb81859)
* cache-control: new option USE_CONTROL_CACHE_HEADER adds Cache-Control header on requests to registry server (#265) (34fd13d)

[1.6.3]
* Update docker registry UI to 2.3.1 - [changes](https://github.com/Joxit/docker-registry-ui/releases/tag/2.3.1)

[1.6.4]
* Update docker registry UI to 2.3.2 -  [changes](https://github.com/Joxit/docker-registry-ui/releases/tag/2.3.2)
* tag-list: pagination doesn't work properly (#274) (19e96ab)

[1.6.5]
* Update docker registry UI to 2.3.3 - [changes](https://github.com/Joxit/docker-registry-ui/releases/tag/2.3.3)
* taglist: show size with a decimal for images between 1 and 10 (017f662)

[1.7.0]
* Update base image to 4.0.0

[1.7.1]
* Update docker registry UI to 2.4.0 - [changes](https://github.com/Joxit/docker-registry-ui/releases/tag/2.4.0)

[1.8.0]
* Make theme settings customizable

[1.8.1]
* Update docker registry UI to 2.4.1 - [changes](https://github.com/Joxit/docker-registry-ui/releases/tag/2.4.1)

[1.9.0]
* Update docker registry UI to 2.5.0 - [changes](https://github.com/Joxit/docker-registry-ui/releases/tag/2.5.0)
* add option `REGISTRY_SECURED` for registries with Basic Auth (ffb6d14)
* taglist: add new option `TAGLIST_PAGE_SIZE` (#318) (9cfb679)
* taglist: improve visibility of multi-architecture images (#271) (4091baa)
* catalog: add multi level branching in catalog (#319) (affb057)
* branching: add configuration for catalog arborescence (1031034)
* catalog: add `CATALOG_DEFAULT_EXPANDED` to expand repositories by default (#302) (5a34029)
* taglist-order: add new option `TAGLIST_ORDER` (#307) (d2222be)

[1.10.0]
* Fix bug where various environment variables were not replaced into static ui

[1.10.1]
* Update docker registry UI to 2.5.1 - [changes](https://github.com/Joxit/docker-registry-ui/releases/tag/2.5.1)
* shift-click for multi-delete-in-a-row selects wrong images (#323) (bf9c6c8)

[1.10.2]
* Update docker registry UI to 2.5.2 - [changes](https://github.com/Joxit/docker-registry-ui/releases/tag/2.5.2)
* scrollbar always displayed on safari (#327) (f015187)

[1.10.3]
* Update docker registry UI to 2.5.3 - [changes](https://github.com/Joxit/docker-registry-ui/releases/tag/2.5.3)
* token-auth: check the presence of www-authenticate header before the status code (3414d7b)

[1.11.0]
* Update docker registry to 2.8.3
* Pass BUILDTAGS argument to go build by @marcusirgens in #3926
* Enable Go build tags by @thaJeztah in #4009
* reference: replace deprecated function SplitHostname by @thaJeztah in #4032
* Dont parse errors as JSON unless Content-Type is set to JSON by @thaJeztah in #4054
* update to go 1.20.8 by @thaJeztah in #4056
* Set Content-Type header in registry client ReadFrom by @thaJeztah in #4053

[1.11.1]
* Update docker registry UI to 2.5.4 - [changes](https://github.com/Joxit/docker-registry-ui/releases/tag/2.5.4)
* taglist: refreshing page set all digets to sha256:e3b0c44298... due to missing seesion cache (#337) (de6d09c9)

[1.11.2]
* Update docker registry UI to 2.5.5 - [changes](https://github.com/Joxit/docker-registry-ui/releases/tag/2.5.5)

[1.11.3]
* Update docker registry UI to 2.5.6 - [changes](https://github.com/Joxit/docker-registry-ui/releases/tag/2.5.6)

[1.11.4]
* Update docker registry UI to 2.5.7
* [Full changelog](https://github.com/Joxit/docker-registry-ui/releases/tag/2.5.7)
* should notify new version only once a day even when an error occurs (#353) (6c3c27e)

[1.11.5]
* Rename app title

[1.12.0]
* Generate http.secret on first run

